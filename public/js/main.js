/**
 * Created by canngo on 1/29/16.
 */
function showLastLogin(miliseconds){
    var periodSeconds = Math.floor((Date.now()/1000-parseInt(miliseconds)));
    var numdays = Math.floor(periodSeconds / 86400);
    var numhours = Math.floor((periodSeconds % 86400) / 3600);
    var numminutes = Math.floor(((periodSeconds % 86400) % 3600) / 60);
    var numseconds = ((periodSeconds % 86400) % 3600) % 60;

    var status ;
    if(numdays >0){
        status = (numdays ==1) ? '1 day ago' : '' + numdays +' days ago';
    } else if (numhours > 0){
        status = (numhours ==1) ? '1 hour ago' : '' + numhours +' hours ago';
    } else if (numminutes > 0){
        status = (numminutes ==1) ? '1 minute ago' : '' + numminutes +' minutes ago';
    } else if (numseconds > 0){
        status = (numseconds ==1) ? 'Just now' : '' + numseconds +' seconds ago';
    }

    status = 'Last login '+status;

    $("#last-time-login").text(status);
}
