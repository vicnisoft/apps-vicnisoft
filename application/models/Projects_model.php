<?php

/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 2/22/16
 * Time: 3:19 PM
 */

include "Entities/Projects.php";

class Projects_model extends MY_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function findById($id = null){
        $result = $this->em->find('Projects', $id);
        if($result != null){
            return $this->toArray($result);
        }
        return $result;
    }

    public function findAllProjectsOrderBy($orderBy = null, $asc = true){
        $projectsArray = null;
        if($orderBy != null){
            $ascending = ($asc == true) ? 'asc' : 'desc';
            $projectsArray= $this->em->getRepository('Projects')->findBy(array('recycled' => 0), array($orderBy=>$ascending));
        } else {
            $projectsArray= $this->em->getRepository('Projects')->findAll();

        }
        $result = array();
        foreach($projectsArray as $projects){
            $result[] = $this->toArray($projects);
        }

        return $result;

    }

    public function insert($name = '', $icon = '', $desc = ''){
        try{
            $this->em->getConnection()->beginTransaction();
            $newProject = new Projects();
            $newProject->setName(trim($name));
            $newProject->setProjectIcon(trim($icon));
            $newProject->setDescription(trim($desc));
            $newProject->setCreatedOn(now());
            $this->em->persist($newProject);
            $this->em->flush();
            $this->em->getConnection()->commit();
            return true;

        } catch(Exception $e){
            $this->em->getConnection()->rollback();
            return false;
        }
    }

    public function update($id = null,$name = '', $icon = '', $desc = ''){
        if($id == null){
            return false;
        }
        try{
            $this->em->getConnection()->beginTransaction();
            $project = $this->em->find('Projects', $id);
            $project->setName(trim($name));
            $project->setProjectIcon(trim($icon));
            $project->setDescription(trim($desc));
            $this->em->persist($project);
            $this->em->flush();
            $this->em->getConnection()->commit();
            return true;

        } catch(Exception $e){
            $this->em->getConnection()->rollback();
            return false;
        }
    }

    public function  delete($id = null){
        if($id == null){
            return false;
        }
        try{
            $this->em->getConnection()->beginTransaction();
            $project = $this->em->find('Projects', $id);
            $project->setRecycled(true);
            $this->em->persist($project);
            $this->em->flush();
            $this->em->getConnection()->commit();
            return true;

        } catch(Exception $e){
            $this->em->getConnection()->rollback();
            return false;
        }
    }

    public function  deleteForever($id = null){
        if($id == null){
            return false;
        }
        try{
            $this->em->getConnection()->beginTransaction();
            $project = $this->em->find('Projects', $id);
            $this->em->remove($project);
            $this->em->flush();
            $this->em->getConnection()->commit();
            return true;

        } catch(Exception $e){
            $this->em->getConnection()->rollback();
            return false;
        }
    }


    private function toArray(Projects $project){
        $proj = array();
        $proj['id'] = $project->getId();
        $proj['name'] = $project->getName();
        $proj['description'] = $project->getDescription();
        $proj['short_description'] = substr($project->getDescription(),0,100);
        $proj['created_on'] = $project->getCreatedOn();
        $proj['project_icon'] = $project->getProjectIcon();
        $proj['recycled'] = $project->getRecycled();
        $proj['edit_url'] = array('project','edit',$proj['id']);

        return $proj;
    }



}