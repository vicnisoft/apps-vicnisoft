<?php

/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 2/19/16
 * Time: 3:28 PM
 */

include 'Entities/Users.php';

class User_model extends MY_Model
{

    /**
     *
     */
    public function __construct() {
        parent::__construct();

    }

    public function findById($id = null){
        $result = $this->em->find('Users', $id);
        if($result != null){
            return $this->toArray($result);
        }
        return $result;
    }

    public function insert($username, $password, $firstname, $lastname, $email, $phone, $ipAddress){
        try{
            $this->em->getConnection()->beginTransaction();
            $users = new Users();
            $users->setUsername($username);
            $users->setPassword($password);
            $users->setIpAddress($ipAddress);
            $users->setEmail($email);
            $users->setPhone($phone);
            $users->setFirstName($firstname);
            $users->setLastName($lastname);
            $users->setCreatedOn(now());
            $this->em->persist($users);
            $this->em->flush();
            $this->em->getConnection()->commit();
            return true;
        } catch(Exception $e){
            $this->em->getConnection()->rollback();
            return false;
        }

    }



}