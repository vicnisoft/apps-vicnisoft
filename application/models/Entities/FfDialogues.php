<?php


include 'FfCategories.php';

use Doctrine\ORM\Mapping as ORM;

/**
 * FfDialogues
 *
 * @ORM\Table(name="ff_dialogues", uniqueConstraints={@ORM\UniqueConstraint(name="index_updated_by", columns={"updated_by"})}, indexes={@ORM\Index(name="index_category_id", columns={"category_id"}), @ORM\Index(name="index_inspector_id", columns={"inspector_id"}), @ORM\Index(name="index_created_by", columns={"created_by"})})
 * @ORM\Entity
 */
class FfDialogues
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", length=65535, nullable=false)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="icon_url", type="string", length=255, nullable=false)
     */
    private $iconUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="audio_url", type="string", length=255, nullable=false)
     */
    private $audioUrl;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_on", type="integer", nullable=false)
     */
    private $createdOn;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_by", type="integer", nullable=false)
     */
    private $createdBy;

    /**
     * @var boolean
     *
     * @ORM\Column(name="recycled", type="boolean", nullable=false)
     */
    private $recycled;

    /**
     * @var integer
     *
     * @ORM\Column(name="updated_on", type="integer", nullable=true)
     */
    private $updatedOn;

    /**
     * @var boolean
     *
     * @ORM\Column(name="approved", type="boolean", nullable=false)
     */
    private $approved;

    /**
     * @var \Users
     *
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="inspector_id", referencedColumnName="id")
     * })
     */
    private $inspector;

    /**
     * @var \FfCategories
     *
     * @ORM\ManyToOne(targetEntity="FfCategories")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;

    /**
     * @var \Users
     *
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     * })
     */
    private $updatedBy;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return FfDialogues
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return FfDialogues
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set iconUrl
     *
     * @param string $iconUrl
     *
     * @return FfDialogues
     */
    public function setIconUrl($iconUrl)
    {
        $this->iconUrl = $iconUrl;

        return $this;
    }

    /**
     * Get iconUrl
     *
     * @return string
     */
    public function getIconUrl()
    {
        return $this->iconUrl;
    }

    /**
     * Set audioUrl
     *
     * @param string $audioUrl
     *
     * @return FfDialogues
     */
    public function setAudioUrl($audioUrl)
    {
        $this->audioUrl = $audioUrl;

        return $this;
    }

    /**
     * Get audioUrl
     *
     * @return string
     */
    public function getAudioUrl()
    {
        return $this->audioUrl;
    }

    /**
     * Set createdOn
     *
     * @param integer $createdOn
     *
     * @return FfDialogues
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return integer
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set createdBy
     *
     * @param integer $createdBy
     *
     * @return FfDialogues
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return integer
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set recycled
     *
     * @param boolean $recycled
     *
     * @return FfDialogues
     */
    public function setRecycled($recycled)
    {
        $this->recycled = $recycled;

        return $this;
    }

    /**
     * Get recycled
     *
     * @return boolean
     */
    public function getRecycled()
    {
        return $this->recycled;
    }

    /**
     * Set updatedOn
     *
     * @param integer $updatedOn
     *
     * @return FfDialogues
     */
    public function setUpdatedOn($updatedOn)
    {
        $this->updatedOn = $updatedOn;

        return $this;
    }

    /**
     * Get updatedOn
     *
     * @return integer
     */
    public function getUpdatedOn()
    {
        return $this->updatedOn;
    }

    /**
     * Set approved
     *
     * @param boolean $approved
     *
     * @return FfDialogues
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;

        return $this;
    }

    /**
     * Get approved
     *
     * @return boolean
     */
    public function getApproved()
    {
        return $this->approved;
    }

    /**
     * Set inspector
     *
     * @param \Users $inspector
     *
     * @return FfDialogues
     */
    public function setInspector(\Users $inspector = null)
    {
        $this->inspector = $inspector;

        return $this;
    }

    /**
     * Get inspector
     *
     * @return \Users
     */
    public function getInspector()
    {
        return $this->inspector;
    }

    /**
     * Set category
     *
     * @param \FfCategories $category
     *
     * @return FfDialogues
     */
    public function setCategory(\FfCategories $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \FfCategories
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set updatedBy
     *
     * @param \Users $updatedBy
     *
     * @return FfDialogues
     */
    public function setUpdatedBy(\Users $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \Users
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }
}
