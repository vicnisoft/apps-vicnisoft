<?php

/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 2/26/16
 * Time: 10:13 AM
 */

include_once 'Entities/FfDialogues.php';


class FFDialogues_model extends MY_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function findById($id = null){
        $result = $this->em->find('FfDialogues', $id);
        if($result != null){
            return $this->toArray($result);
        }
        return $result;
    }

    public function findAllDialoguesOrderBy($orderBy = null, $asc = true){
        $objs = null;
        if($orderBy != null){
            $ascending = ($asc == true) ? 'asc' : 'desc';
            $objs= $this->em->getRepository('FfDialogues')->findBy(array('recycled' => 0), array($orderBy=>$ascending));
        } else {
            $objs= $this->em->getRepository('FfDialogues')->findAll();

        }
        $result = array();
        foreach($objs as $obj){
            $result[] = $this->toArray($obj);
        }

        return $result;

    }

    public function insert($info = array()){
        try{
            $this->em->getConnection()->beginTransaction();
            $newObj = new FfDialogues();
            $newObj->setName(trim($info['name']));
            $newObj->setIconUrl(trim($info['icon']));
            $newObj->setCreatedOn(now());
            $newObj->setRecycled(false);
            $category = $this->em->find('FfCategories', $info['category_id']);
            $newObj->setCategory($category);
            $createdBy = $this->em->find('Users', $info['user_id']);
            $newObj->setCreatedBy($createdBy->getId());
            $newObj->setContent(trim($info['content']));
            $newObj->setAudioUrl(trim($info['audio_url']));
            $newObj->setApproved(false);
            $this->em->persist($newObj);
            $this->em->flush();
            $this->em->getConnection()->commit();
            return true;

        } catch(Exception $e){
            $this->em->getConnection()->rollback();
            return false;
        }
    }

    public function update($info = array()){
        if($info['dialogue_id'] === null){
            return false;
        }
        try{
            $this->em->getConnection()->beginTransaction();
            $anObj = $this->em->find('FfDialogues', $info['dialogue_id']);
            $anObj->setName(trim($info['name']));
            $anObj->setIconUrl(trim($info['icon']));
            $anObj->setUpdatedOn(now());
            $anObj->setRecycled(false);
            $category = $this->em->find('FfCategories', $info['category_id']);
            $anObj->setCategory($category);
            $updatedBy = $this->em->find('Users', $info['user_id']);
            $anObj->setUpdatedBy($updatedBy);
            $anObj->setContent(trim($info['content']));
            $anObj->setAudioUrl(trim($info['audio_url']));
            $this->em->persist($anObj);
            $this->em->flush();
            $this->em->getConnection()->commit();
            return true;

        } catch(Exception $e){
            $this->em->getConnection()->rollback();
            return false;
        }
    }

    public function  delete($id = null){
        if($id == null){
            return false;
        }
        try{
            $this->em->getConnection()->beginTransaction();
            $anObj = $this->em->find('FfDialogues', $id);
            $anObj->setRecycled(true);
            $this->em->persist($anObj);
            $this->em->flush();
            $this->em->getConnection()->commit();
            return true;

        } catch(Exception $e){
            $this->em->getConnection()->rollback();
            return false;
        }
    }

    public function  deleteForever($id = null){
        if($id == null){
            return false;
        }
        try{
            $this->em->getConnection()->beginTransaction();
            $anObj = $this->em->find('FfDialogues', $id);
            $this->em->remove($anObj);
            $this->em->flush();
            $this->em->getConnection()->commit();
            return true;

        } catch(Exception $e){
            $this->em->getConnection()->rollback();
            return false;
        }
    }


    private function toArray(FFDialogues $obj){
        $anObj = array();
        $anObj['id'] = $obj->getId();
        $anObj['name'] = $obj->getName();
        $anObj['content'] = $obj->getContent();
        $anObj['short_content'] = substr($obj->getContent(),0,100);
        $anObj['created_on'] = $obj->getCreatedOn();
        $anObj['created_by'] = $obj->getCreatedBy();
        if($obj->getInspector() !== null){
            $anObj['inspector_id'] = $obj->getInspector()->getId();
            $anObj['inspector_username'] = $obj->getInspector()->getUsername();
        }
        $anObj['audio_url'] = $obj->getAudioUrl();
        $anObj['updated_on'] = $obj->getUpdatedOn();
        $anObj['approved'] = $obj->getApproved();
        if($obj->getCategory() !== null){
            $anObj['category_id'] = $obj->getCategory()->getId();
            $anObj['category_name'] = $obj->getCategory()->getName();
        }
        $anObj['icon'] = $obj->getIconUrl();
        $anObj['recycled'] = $obj->getRecycled();
        $anObj['edit_url'] = array('dialogue','edit',$anObj['id']);

        return $anObj;
    }
}