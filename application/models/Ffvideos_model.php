<?php

/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 2/26/16
 * Time: 10:14 AM
 */
class FFVideos_model extends MY_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function findById($id = null){
        $result = $this->em->find('FfCategories', $id);
        if($result != null){
            return $this->toArray($result);
        }
        return $result;
    }

    public function findAllCategoriesOrderBy($orderBy = null, $asc = true){
        $categoryArray = null;
        if($orderBy != null){
            $ascending = ($asc == true) ? 'asc' : 'desc';
            $categoryArray= $this->em->getRepository('FfCategories')->findBy(array('recycled' => 0), array($orderBy=>$ascending));
        } else {
            $categoryArray= $this->em->getRepository('FfCategories')->findAll();

        }
        $result = array();
        foreach($categoryArray as $category){
            $result[] = $this->toArray($category);
        }

        return $result;

    }

    public function insert($name = '', $icon = '', $desc = ''){
        try{
            $this->em->getConnection()->beginTransaction();
            $newObj = new FfCategories();
            $newObj->setName(trim($name));
            $newObj->setIconUrl(trim($icon));
            $newObj->setDescription(trim($desc));
            $newObj->setCreatedOn(now());
            $newObj->setRecycled(false);
            $this->em->persist($newObj);
            $this->em->flush();
            $this->em->getConnection()->commit();
            return true;

        } catch(Exception $e){
            $this->em->getConnection()->rollback();
            return false;
        }
    }

    public function update($id = null,$name = '', $icon = '', $desc = ''){
        if($id == null){
            return false;
        }
        try{
            $this->em->getConnection()->beginTransaction();
            $anObj = $this->em->find('FfCategories', $id);
            $anObj->setName(trim($name));
            $anObj->setIconUrl(trim($icon));
            $anObj->setDescription(trim($desc));
            $this->em->persist($anObj);
            $this->em->flush();
            $this->em->getConnection()->commit();
            return true;

        } catch(Exception $e){
            $this->em->getConnection()->rollback();
            return false;
        }
    }

    public function  delete($id = null){
        if($id == null){
            return false;
        }
        try{
            $this->em->getConnection()->beginTransaction();
            $anObj = $this->em->find('FfCategories', $id);
            $anObj->setRecycled(true);
            $this->em->persist($anObj);
            $this->em->flush();
            $this->em->getConnection()->commit();
            return true;

        } catch(Exception $e){
            $this->em->getConnection()->rollback();
            return false;
        }
    }

    public function  deleteForever($id = null){
        if($id == null){
            return false;
        }
        try{
            $this->em->getConnection()->beginTransaction();
            $anObj = $this->em->find('FfCategories', $id);
            $this->em->remove($anObj);
            $this->em->flush();
            $this->em->getConnection()->commit();
            return true;

        } catch(Exception $e){
            $this->em->getConnection()->rollback();
            return false;
        }
    }


    private function toArray(FfCategories $obj){
        $anObj = array();
        $anObj['id'] = $obj->getId();
        $anObj['name'] = $obj->getName();
        $anObj['description'] = $obj->getDescription();
        $anObj['short_description'] = substr($obj->getDescription(),0,100);
        $anObj['created_on'] = $obj->getCreatedOn();
        $anObj['icon'] = $obj->getIconUrl();
        $anObj['recycled'] = $obj->getRecycled();
        $anObj['edit_url'] = array('category','edit',$anObj['id']);

        return $anObj;
    }
}