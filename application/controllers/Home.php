<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 1/28/16
 * Time: 2:02 AM
 */
class Home extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

    }

    public function index(){

        $this->twig->display('twig/pages/home', $this->data);
    }

}