<?php

/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 2/22/16
 * Time: 11:41 PM
 */
class Category extends MY_Controller
{
    function __construct(){
        parent::__construct();
        $this->load->model('ffcategories_model');
    }


    public function index() {

        $categories = $this->ffcategories_model->findAllCategoriesOrderBy('name', true);
        $this->data['categories'] = $categories;
        $data = array('recycle_action' => site_url('category/recycle'));
        $this->data['data'] = $data;
        $this->data['page_title'] = "Manage Categories";
        $this->twig->display('twig/francaisfacile/list-category', $this->data);
    }

    public function add() {
        $categoryInfo = $this->input->post('category');
        $name = $categoryInfo['name'];
        if($name != null){
            $icon = $categoryInfo['icon'];
            $description = $categoryInfo['description'];
            $result = $this->ffcategories_model->insert($name, $icon, $description);
            if($result == true){
                $this->data['success_masage'] = "Category $name has been added";
            } else{
                $this->data['error_message'] = "Category $name has not been added";
            }
        }
        $this->data['page_title'] = "Add New Category";
        $this->twig->display('twig/francaisfacile/edit-category', $this->data);
    }

    public function edit($id = null) {
        $categoryInfo = $this->input->post('category');
        $name = $categoryInfo['name'];
        if($name != null){
            $id = $categoryInfo['id'];
            $icon = $categoryInfo['icon'];
            $desc = $categoryInfo['description'];
            $result = $this->ffcategories_model->update($id,$name, $icon, $desc);
            if($result == true){
                $this->data['success_masage'] = "Category $name has been updated";
            } else{
                $this->data['error_message'] = "Category $name has not been updated";
            }
        }
        $category = $this->ffcategories_model->findById($id);
        if($category != null){
            $this->data['category'] = $category;
        }
        $this->data['page_title'] = "Update Category";
        $this->twig->display('twig/francaisfacile/edit-category', $this->data);
    }

    public function recycle(){

        $recycleid = $this->input->post('recycleid');
        if($recycleid !== null){
            $result = $this->ffcategories_model->delete($recycleid);
            if($result == true){
                $this->data['success_masage'] = "Category $recycleid has been deleted";
            } else{
                $this->data['error_message'] = "Category $recycleid has not been deteted";
            }
        }

        redirect('/category/index');
    }

    public function getFromServer(){

        $jsonData = $this->get_remote_data('http://nccntv.xyz/products/francaisfacile/index.php/api/categories');
        $remoteData = json_decode($jsonData);
        $cats = array();

        if($remoteData->success){
            foreach($remoteData->data as $cat){
                $cats[]= $this->ffcategories_model->getCategoryWithArgs($cat);
            }
        }
        $this->data['is_temp'] = true;
        $this->data['categories'] = $cats;
        $data = array('recycle_action' => site_url('category/recycle'));
        $this->data['data'] = $data;
        $this->data['page_title'] = "Manage Categories";
        $this->twig->display('twig/francaisfacile/list-category', $this->data);
    }

    private function  get_remote_data($url, $post_paramtrs = false) {
        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        if ($post_paramtrs) {
            curl_setopt($c, CURLOPT_POST, TRUE);
            curl_setopt($c, CURLOPT_POSTFIELDS, "var1=bla&" . $post_paramtrs);
        } curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($c, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; rv:33.0) Gecko/20100101 Firefox/33.0");
        curl_setopt($c, CURLOPT_COOKIE, 'CookieName1=Value;');
        curl_setopt($c, CURLOPT_MAXREDIRS, 10);
        $follow_allowed = ( ini_get('open_basedir') || ini_get('safe_mode')) ? false : true;
        if ($follow_allowed) {
            curl_setopt($c, CURLOPT_FOLLOWLOCATION, 1);
        }curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 9);
        curl_setopt($c, CURLOPT_REFERER, $url);
        curl_setopt($c, CURLOPT_TIMEOUT, 60);
        curl_setopt($c, CURLOPT_AUTOREFERER, true);
        curl_setopt($c, CURLOPT_ENCODING, 'gzip,deflate');
        $data = curl_exec($c);
        $status = curl_getinfo($c);
        curl_close($c);
        preg_match('/(http(|s)):\/\/(.*?)\/(.*\/|)/si', $status['url'], $link);
        $data = preg_replace('/(src|href|action)=(\'|\")((?!(http|https|javascript:|\/\/|\/)).*?)(\'|\")/si', '$1=$2' . $link[0] . '$3$4$5', $data);
        $data = preg_replace('/(src|href|action)=(\'|\")((?!(http|https|javascript:|\/\/)).*?)(\'|\")/si', '$1=$2' . $link[1] . '://' . $link[3] . '$3$4$5', $data);
        if ($status['http_code'] == 200) {
            return $data;
        } elseif ($status['http_code'] == 301 || $status['http_code'] == 302) {
            if (!$follow_allowed) {
                if (empty($redirURL)) {
                    if (!empty($status['redirect_url'])) {
                        $redirURL = $status['redirect_url'];
                    }
                } if (empty($redirURL)) {
                    preg_match('/(Location:|URI:)(.*?)(\r|\n)/si', $data, $m);
                    if (!empty($m[2])) {
                        $redirURL = $m[2];
                    }
                } if (empty($redirURL)) {
                    preg_match('/href\=\"(.*?)\"(.*?)here\<\/a\>/si', $data, $m);
                    if (!empty($m[1])) {
                        $redirURL = $m[1];
                    }
                } if (!empty($redirURL)) {
                    $t = debug_backtrace();
                    return call_user_func($t[0]["function"], trim($redirURL), $post_paramtrs);
                }
            }
        } return "ERRORCODE22 with $url!!<br/>Last status codes<b/>:" . json_encode($status) . "<br/><br/>Last data got<br/>:$data";
    }
}